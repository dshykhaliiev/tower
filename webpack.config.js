const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const WebpackFreeTexPacker = require('webpack-free-tex-packer');

const getPlugin = function(env) {
    const sources = [
        path.resolve(__dirname, 'trash'),
    ]

    const packOptions = {
        "textureName": "atlas",
        "textureFormat": "png",
        "removeFileExtension": true,
        "prependFolderName": false,
        "base64Export": false,
        "tinify": true,
        "tinifyKey": "Sq32rTCQr1qMgHvMfK1FhkPGRZGVDQjC",
        "scale": 1,
        "disableSmoothing": false,
        "filter": "none",
        "exporter": "Pixi",
        "fileName": "pack-result",
        "width": 2048,
        "height": 2048,
        "fixedSize": false,
        "powerOfTwo": false,
        "padding": 2,
        "extrude": 0,
        "allowRotation": true,
        "allowTrim": true,
        "trimMode": "trim",
        "alphaThreshold": "0",
        "detectIdentical": true,
        "packer": "OptimalPacker",
        "packerMethod": "Automatic",
    }

    if (env.atlas == 'true') {
        console.log("Pack atlas options: ", packOptions)
    }

    const plugins = {
        devServer: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                title: 'template_js_2d',
                template: path.join(__dirname, 'templates/index.html'),
            }),
            new ScriptExtHtmlWebpackPlugin({
                inline: ['bundle.js']
            }),
            new webpack.DefinePlugin({
                'process.env.PRODUCTION': JSON.stringify(false),
            })
        ],

        production: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                title: 'template_js_2d',
                template: path.join(__dirname, 'templates/index.html'),
            }),
            new ScriptExtHtmlWebpackPlugin({
                inline: ['bundle.js']
            }),
            new UglifyJSPlugin({
                uglifyOptions: {
                    warnings: false,
                    compress: {
                        properties: false
                    }
                }
            }),
            new webpack.DefinePlugin({
                'process.env.PRODUCTION': JSON.stringify(true),
            })
        ]
    };

    if (env.atlas === 'true') {
        plugins[env.mode].push(new WebpackFreeTexPacker(sources, `../assets/atlases`, packOptions))
    }

    return plugins[env.mode];
}

module.exports = function(env, args) {
    return {
        entry: {
            app: './src/index.js'
        },

        output: {
            path: path.resolve(__dirname, 'public'),
            filename: 'bundle.js'
        },

        devtool: env.mode === 'devServer' ? 'inline-source-map' : '',

        module: {
            rules: [
                {
                    test: /\.css$/i,
                    use: ["style-loader", "css-loader"],
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    include: path.resolve(__dirname, 'src/'),
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env']
                        }
                    }
                },
                {
                    test: /\.(jpe?g|png|webp|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                    use: 'base64-inline-loader?name=[name].[ext]'
                },
                {
                    test: /\.(mp3?)(\?[a-z0-9=&.]+)?$/,
                    use: 'base64-inline-loader?name=assets/audio/[name].[ext]'
                },
                {
                    test: /\.(ttf?)(\?[a-z0-9=&.]+)?$/,
                    use: 'base64-inline-loader?name=[name].[ext]'
                }
            ]
        },
        plugins: getPlugin(env),

        devServer: (env.mode === 'devServer') ? {
            contentBase: path.join(__dirname, 'public'),
            compress: false,
            port: 8080,
            inline: true,
            overlay: true
        } : {},
        watch: env.mode === 'devServer',
    };
}