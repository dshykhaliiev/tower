import * as PIXI from 'pixi.js';

const utils = {
    createCircle: (color, radius) => {
        const circle = new PIXI.Graphics();
        circle.beginFill(color);
        circle.drawCircle(0, 0, radius);
        circle.endFill();

        return circle;
    },

    getRandomInt(from, to) {
        return Math.random() * (to - from) + from;
    },

    hitTest(obj1, obj2) {
        const bounds1 = obj1.getBounds();
        const bounds2 = obj2.getBounds();

        return bounds1.x < bounds2.x + bounds2.width
            && bounds1.x + bounds1.width > bounds2.x
            && bounds1.y < bounds2.y + bounds2.height
            && bounds1.y + bounds1.height > bounds2.y;
    }
}

export { utils }