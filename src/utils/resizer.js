'use strict';

import {DeviceUtils} from "./device-utils";
import * as PIXI from 'pixi.js';
import { TimeoutUtils } from './timeout-utils';

class Resizer {
    constructor(app, gameWidth, gameHeight) {
        this._width = gameWidth;
        this._height = gameHeight;

        this._gameWidth = gameWidth;
        this._gameHeight = gameHeight;

        this._isPortrait = true;
        this._scale = 1;

        this.callbacks = {};

        console.log('init resizer');

        this.app = app;
        this.view = app.view;

        window.addEventListener('resize', () => {
            this.fitRendererToWindow();
        });

        this.fitRendererToWindow();

        this.createDebug();
    }

    createDebug() {
        // TODO: For debug purpose only
        // this.createIPhoneDebug();
        // this.createIPadDebug();
    }

    createIPadDebug() {
        if (this.iPad) {
            this.iPad.destroy();
        }

        const squareSize = 10;

        const iPad = new PIXI.Graphics();
        iPad.beginFill(0x0000FF, 0.3);
        iPad.drawRect(-384, -512, 768, 1024);
        iPad.beginFill(0x00FF00, 1);
        iPad.drawRect(-384, -512, 10, 10);
        iPad.drawRect(384 - squareSize, -512, 10, 10);
        iPad.drawRect(384 - squareSize, 512 - squareSize, 10, 10);
        iPad.drawRect(-384, 512  -squareSize, 10, 10);
        iPad.endFill();

        iPad.height = this.height;
        iPad.scale.x = iPad.scale.y;

        this.iPad = iPad;

        TimeoutUtils.setInterval(this.updateIPadDebug.bind(this), 1000);
    }

    createIPhoneDebug() {
        if (this.iPhone) {
            this.iPhone.destroy();
        }

        const squareSize = 10;
        const scrW = 375;
        const scrH = 812;

        const iPhone = new PIXI.Graphics();
        iPhone.beginFill(0x00FFF2, 0.35);
        iPhone.drawRect(-scrW / 2, -scrH / 2, scrW, scrH);

        iPhone.beginFill(0xFF0000, 1);
        iPhone.drawRect(-scrW / 2, -scrH / 2, 10, 10);
        iPhone.drawRect(scrW / 2 - squareSize, -scrH / 2, 10, 10);
        iPhone.drawRect(scrW / 2 - squareSize, scrH / 2 - squareSize, 10, 10);
        iPhone.drawRect(-scrW / 2, scrH / 2 - squareSize, 10, 10);
        iPhone.endFill();

        iPhone.height = this.height;
        iPhone.scale.x = iPhone.scale.y;
        this.iPhone = iPhone;

        TimeoutUtils.setInterval(this.updateIPhoneDebug.bind(this), 1000);
    }

    updateIPhoneDebug() {
        this.app.stage.addChild(this.iPhone);
    }

    updateIPadDebug() {
        this.app.stage.addChild(this.iPad);
    }

    onResize(callback) {
        this.callbacks[callback] = callback;
    }

    get width() {
        return this._width / this._scale;
    }

    get height() {
        return this._height / this._scale;
    }

    get isPortrait() {
        return this._isPortrait;
    }

    get scale() {
        return this._scale;
    }

    get screenSize() {
        return {
            width: this.width,
            height: this.height,
            isPortrait: this.isPortrait
        }
    }

    fitRendererToWindow() {
        const getInnerWidth = () => {
            return DeviceUtils.isDesktop() ? 300 : window.innerWidth;
        }

        const getInnerHeight = () => {
            return DeviceUtils.isDesktop()  ? 649.6 : window.innerHeight;
        }

        this._width = getInnerWidth();
        this._height = getInnerHeight();

        // landscape
        if (!DeviceUtils.isDesktop() && window.innerWidth > window.innerHeight) {
            this._isPortrait = false;

            const scale = (window.innerWidth / this._gameWidth);
            this.app.stage.scale.set(scale, scale);
            this._scale = scale;
        }
        else { // portrait
            this._isPortrait = true;
            const scale = (getInnerHeight() / this._gameHeight);
            this.app.stage.scale.set(scale, scale);
            this._scale = scale;
        }

        this.app.stage.x = getInnerWidth() * .5;
        this.app.stage.y = getInnerHeight() * .5;

        this.app.renderer.resize(getInnerWidth(), getInnerHeight());

        for (let key in this.callbacks) {
            const callback = this.callbacks[key];
            callback(this.screenSize);
        }
    }
}

export {
    Resizer
};