class SoundManager {
    constructor() {
        const AudioContext = window.AudioContext || window.webkitAudioContext;
        const audioCtx = new AudioContext();

        SoundManager.audioCtx = audioCtx;
    }

    static playSound(sound, isLoop = false, volume = 1) {
        const audioCtx = SoundManager.audioCtx;
        let gainNode = audioCtx.createGain();
        SoundManager.gainNode = gainNode;
        gainNode.connect(audioCtx.destination);
        gainNode.gain.value = volume;

        let source;
        source = audioCtx.createBufferSource();
        source.buffer = SoundManager.assets.audio[sound];
        source.loop = isLoop;
        source.start();
        source.connect(gainNode);

        return {gainNode, source};
    }

    static pauseAllSounds() {
        SoundManager.audioCtx.suspend();
    }

    static resumeAllSounds() {
        SoundManager.audioCtx.resume();
    }
}

export {
    SoundManager
}