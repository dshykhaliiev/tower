class GameEventsEnum {
	static get PLAYER_JUMP_START() {
		return 'PLAYER_JUMP_START';
	}

	static get PLAYER_JUMP_FINISHED() {
		return 'PLAYER_JUMP_FINISHED';
	}

	static get GAME_FIELD_STOPPED_MOVING() {
		return 'GAME_FIELD_STOPPED_MOVING';
	}

	static get PLAYER_DIED() {
		return 'PLAYER_DIED';
	}

	static get ZOOM_OUT_COMPLETE() {
		return 'ZOOM_OUT_COMPLETE';
	}
}

export { GameEventsEnum };