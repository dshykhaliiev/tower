import * as PIXI from 'pixi.js';
import { config } from '../../model/config';

class Block extends PIXI.Container {
	constructor(assets, id) {
		super();

		this._id = id;
		this.assets = assets;
		this._isActive = true;
		this._isTouched = false;
		this._speed = 0;

		this.init();
	}

	get id() {
		return this._id;
	}

	init() {
		const block = new PIXI.Sprite(this.assets.atlases.atlas.textures.block);
		block.anchor.set(0.5, 0);
		block.height = config.tower.blockHeight;
		block.width = config.tower.blockWidth;
		this.addChild(block);
	}

	set isActive(value) {
		this._isActive = value;
	}

	get isActive() {
		return this._isActive;
	}

	set isTouched(value) {
		this._isTouched = value;
	}

	get isTouched() {
		return this._isTouched;
	}

	set speed(value) {
		this._speed = value;
	}

	get speed() {
		return this._speed;
	}
}

export { Block }