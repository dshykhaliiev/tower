import * as PIXI from 'pixi.js';
import { config } from '../../model/config';
import { Block } from './block';

class Tower extends PIXI.Container {
	constructor(assets) {
		super();

		this.assets = assets;

		this.createPlatform();

		this.blocksWrapper = new PIXI.Container();
		this.blocksWrapper.y = config.tower.blockHeight - config.tower.blocksOverlap;
		this.addChild(this.blocksWrapper);
	}

	createPlatform() {
		this.platform = new PIXI.Sprite(this.assets.atlases.atlas.textures.platform);
		this.platform.anchor.set(0.5, 0);
		this.platform.scale.set(0.4);
		this.addChild(this.platform);
	}

	addBlock() {
		const block = new Block(this.assets, this.blocksWrapper.children.length);
		this.blocksWrapper.addChild(block);
		block.y = -(config.tower.blockHeight - config.tower.blocksOverlap) * this.blocksWrapper.children.length;

		return block;
	}

	removeBlocks() {
		this.blocksWrapper.removeChildren();
	}
}

export { Tower };