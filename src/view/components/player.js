import * as PIXI from 'pixi.js';

class Player extends PIXI.Container {
	constructor(assets) {
		super();

		this.assets = assets;

		this.init();

		this.createSensors();
	}

	init() {
		this.icon = new PIXI.Sprite(this.assets.atlases.atlas.textures.player);
		this.icon.anchor.set(0.5);
		this.icon.scale.set(0.6);
		this.addChild(this.icon);
	}

	createSensors() {
		let graphics = new PIXI.Graphics();
		graphics.beginFill(0x00FFFF, 0.01);
		graphics.drawRect(0, 0, 6, this.icon.height - 10);
		graphics.endFill();

		this.sensorLeft = new PIXI.Sprite();
		this.sensorLeft.addChild(graphics);
		this.sensorLeft.position.set(this.icon.width / 2 - 12, -this.icon.height / 2 - 5);
		this.addChild(this.sensorLeft);

		graphics = new PIXI.Graphics();
		graphics.beginFill(0x00FFFF, 0.01);
		graphics.drawRect(0, 0, 6, this.icon.height - 10);
		graphics.endFill();

		this.sensorRight = new PIXI.Sprite();
		this.sensorRight.addChild(graphics);
		this.sensorRight.position.set(-this.icon.width / 2 + 6, -this.icon.height / 2 - 5);
		this.addChild(this.sensorRight);

		graphics = new PIXI.Graphics();
		graphics.beginFill(0xFFFF00, 0.01);
		graphics.drawRect(0, 0, this.icon.width - 26, 6);
		graphics.endFill();

		this.sensorBottom = new PIXI.Sprite();
		this.sensorBottom.addChild(graphics);
		this.sensorBottom.position.set(-this.icon.width / 2 + 15, this.icon.height / 2 - 5);
		this.addChild(this.sensorBottom);
	}
}

export { Player };