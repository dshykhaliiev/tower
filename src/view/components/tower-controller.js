import { TimeoutUtils } from '../../utils/timeout-utils';
import { utils } from '../../utils/utils';
import { config } from '../../model/config';
import { WIDTH } from '../../game';

class TowerController {
	constructor(tower) {
		this.tower = tower;
		this.blocks = [];
		this.currentBlock = null;

		this.isTowerMoving = false;
		this.currentTowerY = 0;
		this.isStopped = false;
		this.isStarted = false;

		this.init();
	}

	init() {
		this.tower.y = config.player.startPosY;
	}

	start() {
		this.isStarted = true;
		this.addBlock();
	}

	addBlock() {
		if (this.isStopped) {
			return;
		}

		const block = this.tower.addBlock();
		const direction = Math.random() > 0.5 ? 1 : -1;

		block.x = WIDTH * 0.8 * direction;

		this.blocks.push(block);
		this.currentBlock = block;
	}

	update() {
		if (this.currentBlock && this.currentBlock.isActive) {
			if (this.currentBlock.x > 0) {
				this.currentSpeed = -config.tower.blockSpeed;
				this.currentBlock.speed = this.currentSpeed;

				if (this.currentBlock.x <= 0) {
					this.stopCurrentBlock();
				}
			}
			else {
				this.currentSpeed = config.tower.blockSpeed;
				this.currentBlock.speed = this.currentSpeed;

				if (this.currentBlock.x >= 0) {
					this.stopCurrentBlock();
				}
			}

			this.currentBlock.x += this.currentSpeed;
		}

		if (this.tower && this.isTowerMoving) {
			this.tower.y += config.gravity;

			if (this.tower.y >= this.currentTowerY + (config.tower.blockHeight - config.tower.blocksOverlap)) {
				this.tower.isTowerMoving = false;
				this.tower.y = this.currentTowerY + (config.tower.blockHeight - config.tower.blocksOverlap);
			}
		}
	}

	stopCurrentBlock() {
		if (this.currentBlock.isTouched && !this.currentBlock.isActive) {
			return;
		}

		this.currentSpeed = 0;
		this.currentBlock.isActive = false;

		const delay = utils.getRandomInt(config.tower.newBlockMinDelay, config.tower.newBlockMaxDelay);

		if (this.nextBlockTimeout) {
			TimeoutUtils.clearTimeout(this.nextBlockTimeout);
			this.nextBlockTimeout = null;
		}
		this.nextBlockTimeout = TimeoutUtils.setTimeout(this.addBlock, delay, this);
	}

	moveDown() {
		this.isTowerMoving = true;
		this.currentTowerY = this.tower.y;
	}

	stopBlock() {
		this.stopCurrentBlock();
	}

	getCurrentBlock() {
		return this.currentBlock;
	}

	getBlocks() {
		return this.blocks;
	}

	getTowerY() {
		return this.tower.y;
	}

	stop() {
		this.isStopped = true;

		if (this.nextBlockTimeout) {
			TimeoutUtils.clearTimeout(this.nextBlockTimeout);
			this.nextBlockTimeout = null;
		}
	}

	restart() {
		if (this.nextBlockTimeout) {
			TimeoutUtils.clearTimeout(this.nextBlockTimeout);
			this.nextBlockTimeout = null;
		}

		this.tower.removeBlocks();
		this.blocks.length = 0;

		this.currentBlock = null;

		this.isTowerMoving = false;
		this.currentTowerY = 0;
		this.isStopped = false;
		this.isStarted = false;

		this.init();

		this.start();
	}
}

export { TowerController };