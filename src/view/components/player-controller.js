import { SoundManager } from '../../utils/sound-manager';
import { config } from '../../model/config';
import {HEIGHT, WIDTH} from "../../game";
import {utils} from "../../utils/utils";
import * as TWEEN from "tween.js";

class PlayerController {
	constructor(player, resizer) {
		this.player = player;
		this.resizer = resizer;
		this.canJump = true;
		this.acceleration = 0;
		this.isJump = false;

		this.isMovingDown = false;
		this.isDead = false;

		this.setStartPosition();
	}

	setStartPosition() {
		this.player.y = config.player.startPosY;
	}

	jump() {
		if (!this.canJump) {
			return;
		}

		this.isJump = true;
		this.canJump = false;

		SoundManager.playSound('whoosh');
		this.acceleration = config.player.jumpHeight;
	}

	onJumpStart(callback) {
		this._onJumpStartCallback = callback;
	}

	onJumpFinished(callback) {
		this._onJumpFinishedCallback = callback;
	}

	update(blocks, towerY) {
		if (this.isDead || !blocks || blocks.length === 0) {
			return;
		}

		if (this.isJump) {
			this.acceleration += config.gravity;
			this.player.y += this.acceleration;
		}

		const block = blocks[blocks.length - 1];
		if (utils.hitTest(this.player.sensorLeft, block)) {
			this.die(block);
			return;
		}

		if (utils.hitTest(this.player.sensorRight, block)) {
			this.die(block);
			return;
		}


		blocks.forEach((block) => {
			if (utils.hitTest(this.player.sensorBottom, block)) {
				const blockY = block.y + towerY + config.tower.blocksOverlap * 2;
				this.player.y = blockY;
				this.finishJump(false, block);
			}
		});

		if (this.acceleration > 0 && this.player.y >= config.player.startPosY && !this.isDead) {
			this.player.y = config.player.startPosY;
			this.finishJump(true);
		}

		if (this.player && this.isMovingDown && !this.isJump && !this.isDead) {
			this.player.y += config.gravity;

			if (this.player.y >= config.player.startPosY) {
				this.isMovingDown = false;
				this.player.y = config.player.startPosY;
			}
		}
	}

	die(block) {
		this.isDead = true;
		this._onPlayerDiedCallback();

		const multiplier = block.speed > 0 ? 1 : -1;

		if (this.moveXTween) {
			TWEEN.remove(this.moveXTween);
			this.moveXTween = null;
		}

		this.moveXTween = new TWEEN.Tween(this.player)
			.to({x: WIDTH * 0.6 * multiplier, angle: 800 * multiplier}, 1000)
			.easing(TWEEN.Easing.Linear.None)
			.onComplete(() => {
				this.player.visible = false;
			})
			.start();

		if (this.moveYTween) {
			TWEEN.remove(this.moveYTween);
			this.moveYTween = null;
		}

		this.moveYTween = new TWEEN.Tween(this.player)
			.to({y: HEIGHT * 0.6}, 1000)
			.easing(TWEEN.Easing.Sinusoidal.In)
			.start();
	}

	onPlayerDied(callback) {
		this._onPlayerDiedCallback = callback;
	}

	finishJump(isPlatform = false, currentBlock = null) {
		this.isJump = false;
		this.canJump = true;
		this.acceleration = 0;

		this._onJumpFinishedCallback(isPlatform, currentBlock);
	}

	moveDown() {
		this.isMovingDown = true;
	}

	restart() {
		this.canJump = true;
		this.acceleration = 0;
		this.isJump = false;

		this.isMovingDown = false;
		this.isDead = false;

		this.setStartPosition();

		this.player.x = 0;
		this.player.angle = 0;
		this.player.visible = true;

		if (this.moveXTween) {
			TWEEN.remove(this.moveXTween);
			this.moveXTween = null;
		}

		if (this.moveYTween) {
			TWEEN.remove(this.moveYTween);
			this.moveYTween = null;
		}
	}
}

export { PlayerController }