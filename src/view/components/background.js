import * as PIXI from 'pixi.js';
import { HEIGHT, WIDTH } from '../../game';
import { config } from '../../model/config';

class Background extends PIXI.Container {
	constructor(assets, resizer) {
		super();

		this.assets = assets;
		this.resizer = resizer;
		this.acceleration = 0;
		this.isMoving = false;

		this.init();
	}

	init() {
		this.bg = new PIXI.Sprite(this.assets.images.bg.texture);
		this.bg.anchor.set(0.5, 0.5);
		this.bg.height = HEIGHT;
		this.bg.scale.x = this.bg.scale.y;
		this.addChild(this.bg);

		this.stars = new PIXI.Sprite(this.assets.images.stars.texture);
		this.stars.anchor.set(0.5, 0.5);
		this.stars.height = HEIGHT;
		this.stars.scale.x = this.stars.scale.y;
		this.addChild(this.stars);
	}

	moveDown() {
		this.acceleration = 0;
		this.isMoving = true;
	}

	stopMoving() {
		this.isMoving = false;
	}

	restart() {
		this.bg.y = 0;
		this.isMoving = false;
	}

	update() {
		if (!this.isMoving) {
			return;
		}

		this.bg.y += config.gravity;

		if (this.bg.y >= 380) {
			this.bg.y = 180;
		}
	}
}

export {Background};