import * as PIXI from 'pixi.js';
import { Player } from './player';
import { config } from '../../model/config';
import { PlayerController } from './player-controller';
import {Game, HEIGHT, WIDTH} from '../../game';
import { eventBus } from '../../utils/events';
import { GameEventsEnum } from '../../enums/game-events-enum';
import { TowerController } from './tower-controller';
import { Tower } from './tower';
import { TimeoutUtils } from '../../utils/timeout-utils';
import * as TWEEN from "tween.js";

class GameField extends PIXI.Container {
	constructor(assets, resizer) {
		super();

		this.assets = assets;
		this.resizer = resizer;

		this.createTower();

		this.createPlayer();

		this.createHitZone();
	}

	start() {
		this.interactive = true;
		this.on('pointerdown', this.onPointerDown, this);

		this.towerController.start();
	}

	createHitZone() {
		const graphics = new PIXI.Graphics();
		graphics.beginFill(0x00FFFF, 0.01);
		graphics.drawRect(-5, -5, 10, 10);
		graphics.endFill();

		this.addChild(graphics);
		graphics.width = WIDTH;
		graphics.height = HEIGHT;

		this.hitZone = graphics;
	}

	createPlayer() {
		this.player = new Player(this.assets);
		this.addChild(this.player);

		this.playerController = new PlayerController(this.player, this.resizer);
		this.playerController.onJumpFinished(this.onJumpFinished.bind(this));
		this.playerController.onPlayerDied(this.onPlayerDied.bind(this));
	}

	onPlayerDied() {
		this.towerController.stop();
		eventBus.emit(GameEventsEnum.PLAYER_DIED);

		if (this.zoomOutTimeout) {
			TimeoutUtils.clearTimeout(this.zoomOutTimeout);
			this.zoomOutTimeout = null;
		}

		this.zoomOutTimeout = TimeoutUtils.setTimeout(this.zoomOut, 150, this);
	}

	zoomOut() {
		const targetHeight = HEIGHT * 0.8;
		const scaleFactor = (targetHeight / this.tower.height) > 1 ? 1 : targetHeight / this.tower.height;

		if (this.scaleTween) {
			TWEEN.remove(this.scaleTween);
			this.scaleTween = null;
		}
		this.scaleTween = new TWEEN.Tween(this.scale)
			.to({x: scaleFactor, y: scaleFactor}, 1000)
			.easing(TWEEN.Easing.Sinusoidal.In)
			.start();


		if (this.yTween) {
			TWEEN.remove(this.yTween);
			this.yTween = null;
		}
		this.yTween = new TWEEN.Tween(this)
			.to({y: 0}, 1000)
			.easing(TWEEN.Easing.Sinusoidal.In)
			.onComplete(() => {
				eventBus.emit(GameEventsEnum.ZOOM_OUT_COMPLETE);
			})
			.start();
	}

	onJumpFinished(isPlatform, block) {
		if (isPlatform) {
			return;
		}

		const currentBlock = this.towerController.getCurrentBlock();
		if (block !== currentBlock) {
			return;
		}

		if (!currentBlock.isTouched) {
			this.towerController.stopCurrentBlock();

			if (this.jumpFinishedTimeout) {
				TimeoutUtils.clearTimeout(this.jumpFinishedTimeout);
				this.jumpFinishedTimeout = null;
			}

			this.jumpFinishedTimeout = TimeoutUtils.setTimeout(() => {
				eventBus.emit(GameEventsEnum.PLAYER_JUMP_FINISHED);

				if (this.moveDownTween) {
					TWEEN.remove(this.moveDownTween);
					this.moveDownTween = null;
				}

				this.moveDownTween = new TWEEN.Tween(this)
					.to({y: this.y + config.tower.blockHeight - config.tower.blocksOverlap}, 200)
					.onComplete(() => {
						eventBus.emit(GameEventsEnum.GAME_FIELD_STOPPED_MOVING);

						this.hitZone.y = -this.y;
					})
					.start();
			}, 100, this);
		}

		currentBlock.isTouched = true;
	}

	createTower() {
		this.tower = new Tower(this.assets);
		this.addChild(this.tower);

		this.towerController = new TowerController(this.tower);
	}

	onPointerDown() {
		this.playerController.jump();
	}

	update() {
		this.towerController.update();

		const blocks = this.towerController.getBlocks();
		this.playerController.update(blocks, this.towerController.getTowerY());
	}

	restart() {
		if (this.zoomOutTimeout) {
			TimeoutUtils.clearTimeout(this.zoomOutTimeout);
			this.zoomOutTimeout = null;
		}

		if (this.scaleTween) {
			TWEEN.remove(this.scaleTween);
			this.scaleTween = null;
		}

		if (this.yTween) {
			TWEEN.remove(this.yTween);
			this.yTween = null;
		}

		if (this.jumpFinishedTimeout) {
			TimeoutUtils.clearTimeout(this.jumpFinishedTimeout);
			this.jumpFinishedTimeout = null;
		}

		if (this.moveDownTween) {
			TWEEN.remove(this.moveDownTween);
			this.moveDownTween = null;
		}

		this.hitZone.y = 0;

		this.playerController.restart();
		this.towerController.restart();
	}
}

export { GameField };