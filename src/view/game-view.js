import * as PIXI from 'pixi.js';
import {SoundManager} from "../utils/sound-manager";
import { Background } from './components/background';
import { GameField } from './components/game-field';
import { eventBus } from '../utils/events';
import { GameEventsEnum } from '../enums/game-events-enum';
import {HEIGHT, WIDTH} from "../game";
import {DeviceUtils, IOS, ANDROID} from "../utils/device-utils";

class GameView extends PIXI.Container {
    constructor(assets, model, resizer) {
        super();

        this.assets =  assets;
        this.resizer = resizer;
        this.model = model;
        this.logoSize = model.logoSize;
        this.blocksCount = 0;

        this.width = WIDTH;
        this.height = HEIGHT;

        this.createBg();

        this.createGameField();

        this.createTutorial();

        // this.createDebugCenter();

        this.createCounter();

        this.createBtns();

        this.addListeners();
    }

    createBtns() {
        this.restartBtn = new PIXI.Container();
        this.restartBtn.interactive = true;
        this.restartBtn.position.set(-80, 100);
        this.restartBtn.visible = false;
        this.addChild(this.restartBtn);

        let btn = new PIXI.Sprite(this.assets.atlases.atlas.textures.button);
        btn.scale.set(0.3);
        btn.anchor.set(0.5);
        this.restartBtn.addChild(btn);

        let style = { fontSize: 16, align: 'center', fill: '#f3d8a8', fontWeight: 900 };
        let text = new PIXI.Text('Restart', style);
        text.anchor.set(0.5);
        this.restartBtn.addChild(text);

        this.storeBtn = new PIXI.Container();
        this.storeBtn.interactive = true;
        this.storeBtn.position.set(80, 100);
        this.storeBtn.visible = false;
        this.addChild(this.storeBtn);

        btn = new PIXI.Sprite(this.assets.atlases.atlas.textures.button);
        btn.scale.set(0.3);
        btn.anchor.set(0.5);
        this.storeBtn.addChild(btn);

        const label = DeviceUtils.getMobileOS() === IOS ? 'Apple Store' : 'Play market';
        style = { fontSize: 16, align: 'center', fill: '#f3d8a8', fontWeight: 900 };
        text = new PIXI.Text(label, style);
        text.anchor.set(0.5);
        this.storeBtn.addChild(text);
    }

    addListeners() {
        this.interactive = true;
        this.once('pointerdown', (event) => {
            this.removeChild(this.tutorial);
            this.tutorial = null;

            this.gameField.start();
        })

        eventBus.on(GameEventsEnum.PLAYER_JUMP_FINISHED, this.onPlayerJumpFinish, this);
        eventBus.on(GameEventsEnum.GAME_FIELD_STOPPED_MOVING, this.onGameFieldStoppedMoving, this);
        eventBus.on(GameEventsEnum.PLAYER_DIED, this.onPlayerDied, this);
        eventBus.on(GameEventsEnum.ZOOM_OUT_COMPLETE, this.onZoomOutComplete, this);

        this.storeBtn.on('pointerdown', () => {
            console.log('redirect to store');
        });

        this.restartBtn.on('pointerdown', this.restart, this);
    }

    restart() {
        this.storeBtn.visible = false;
        this.restartBtn.visible = false;

        this.blocksCount = 0;

        this.counterText.text = this.blocksCount;

        this.gameField.restart();
    }

    onZoomOutComplete() {
        this.storeBtn.visible = true;
        this.restartBtn.visible = true;
    }

    onPlayerDied() {
        SoundManager.playSound('fail', false, 0.25);
    }

    onGameFieldStoppedMoving() {
        this.bg.stopMoving();
    }

    onPlayerJumpFinish() {
        this.bg.moveDown();
        this.blocksCount++;

        this.counterText.text = this.blocksCount;
    }

    createTutorial() {
        const style = { fontSize: 20, align: 'center', fill: '#f3d8a8', fontWeight: 900 };
        const text = new PIXI.Text('Tap to start', style);
        text.anchor.set(0.5);
        this.addChild(text);

        this.tutorial = text;
    }

    createCounter() {
        const style = { fontSize: 20, align: 'center', fill: '#f3d8a8', fontWeight: 900 };
        const text = new PIXI.Text('0', style);
        text.anchor.set(0.5);
        text.position.set(0, -HEIGHT * 0.5 + 30);
        this.addChild(text);

        this.counterText = text;
    }

    createBg() {
        this.bg = new Background(this.assets, this.resizer);
        this.addChild(this.bg);
    }

    createGameField() {
        this.gameField = new GameField(this.assets, this.resizer);
        this.addChild(this.gameField);
    }

    createDebugCenter() {
        const debugCenter = new PIXI.Graphics();
        debugCenter.beginFill(0x00ff00);
        debugCenter.drawCircle(0, 0, 5);
        debugCenter.endFill();

        // DEBUG PURPOSE ONLY, represents center of the container
        debugCenter.visible = true;

        this.addChild(debugCenter);
    }

    start() {

    }

    update() {
        this.bg.update();
        this.gameField.update();
    }

    resize(screenSize) {
    }
}

export {
    GameView
};