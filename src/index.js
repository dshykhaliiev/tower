import {Game} from "./game";
import './index.css';

window.onload = () => {
    window.removeLoader();

    if (process.env.PRODUCTION) {
        console.log = () => {};
    }

    init();
};

function init() {
    new Game();
}