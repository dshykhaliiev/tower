import * as PIXI from 'pixi.js';
import {Resizer} from "./utils/resizer";
import {GameController} from "./controller/game-controller";
import {SoundManager} from "./utils/sound-manager";
import { DeviceUtils } from './utils/device-utils';

const HEIGHT = 640;
const WIDTH = 480;

class Game extends PIXI.Container {
    constructor() {
        super();

        new SoundManager();

        const app = this.initPixi();
        app.view.addEventListener('touchend', this.enableAudioContext.bind(this));
        this.app = app;

        document.addEventListener("visibilitychange", this.handleVisibilityChange, false);

        this.init();
    }

    init() {
        const resizer = new Resizer(this.app, WIDTH, HEIGHT);
        const controller = new GameController(this.app, resizer);

        resizer.onResize((screenSize) => controller.resize(screenSize));
    }

    initPixi() {
        const app = new PIXI.Application({
            width: DeviceUtils.isDesktop() ? window.innerWidth * 2 : window.innerWidth * devicePixelRatio,
            height: DeviceUtils.isDesktop() ? window.innerWidth * 2 :  window.innerHeight * devicePixelRatio,
            autoResize: true,
            backgroundColor: 0x000000,
            resolution: DeviceUtils.isDesktop() ? 2: devicePixelRatio,
            antialias: true,
        });

        document.body.appendChild(app.view);

        return app;
    }

    handleVisibilityChange() {
        if (document.hidden) {
            SoundManager.pauseAllSounds();
        } else  {
            SoundManager.resumeAllSounds();
        }
    }

    enableAudioContext() {
        this.app.view.removeEventListener('touchend', this.enableAudioContext);
        SoundManager.resumeAllSounds();
    }
}

export {
    Game,
    WIDTH,
    HEIGHT,
};