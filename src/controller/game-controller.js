import {GameView} from "../view/game-view";
import {GameModel} from "../model/game-model";
import {AssetLoader} from "../utils/asset-loader";
import {SoundManager} from "../utils/sound-manager";
import * as TWEEN from "tween.js";
import {config} from "../model/config";

class GameController {
    constructor(app, resizer) {
        this.app = app;
        this.resizer = resizer;

        app.ticker.add(this.update, this);

        this.init();
    }

    init() {
        const assetLoader = new AssetLoader();
        assetLoader
            .load()
            .then(this.saveAssets.bind(this))
            .then(this.initModels.bind(this))
            .then(this.startGame.bind(this));
    }

    saveAssets(assets) {
        SoundManager.assets = assets;
        this.assets = assets;
    }

    initModels() {
        const model = new GameModel();
        model.configObj = config;
        this.model = model;
    }

    startGame() {
        this.gameView = new GameView(this.assets, this.model, this.resizer);
        this.gameView.resize(this.resizer.screenSize);
        this.gameView.start();

        this.app.stage.addChild(this.gameView);
    }

    update(delta) {
        if (this.gameView) {
            this.gameView.update(delta);
        }

        TWEEN.update();
    }

    resize(screenSize) {
        if (this.gameView) {
            this.gameView.resize(screenSize);
        }
    }
}

export {GameController};