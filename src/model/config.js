const config = {
	gravity: 1,
	player: {
		maxSpeed: 3,
		startPosY: 208,
		jumpHeight: -15//-20,
	},
	tower: {
		newBlockMinDelay: 100,
		newBlockMaxDelay: 500,
		blockHeight: 40,
		blockWidth: 90,
		blockSpeed: 4,
		blocksOverlap: 5,
	}
}

export { config }