class GameModel {
    constructor() {
        this.configObj = {};
    }

    static set LANG(value) {
        GameModel.language = value;
    }

    static get LANG() {
        return GameModel.language;
    }

    static set CONFIG(value) {
        GameModel.config = value;
    }

    static get CONFIG() {
        return GameModel.config;
    }
}

export {GameModel};